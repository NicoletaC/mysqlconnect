import java.sql.*;

public class BazaDeDate {
    private static Connection connect = null;
    private static Statement statement = null;
    private static ResultSet resultSet = null;
    private static String url = "jdbc:mysql://localhost:3306/lucrare_practica";
    private static String user = "root", pass = "root";


    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            connect = DriverManager.getConnection(url , user, pass );

            statement = connect.createStatement();

            resultSet = statement.executeQuery("select * from depozit");

            while(resultSet.next()){
                int id_furnizor = resultSet.getInt(4);
                String nume_depozit = resultSet.getString(1);
                String  nume_produs = resultSet.getString(2);
                String cantitate = resultSet.getString(3);
                System.out.println(nume_depozit +  " " + nume_produs +  " " + cantitate +  " " + id_furnizor);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
